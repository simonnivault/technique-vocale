Technique vocale
================

## Introduction

La compétence technique du chanteur se mesure à sa capacité à
deployer un maximum de qualités vocales afin de valoriser
l'œuvre qu'il interprète.
Ces qualités vocales sont mesurables et objectives, leur acuité
ou leur carence vont conditionner les possibilités du chanteur.
 * Force (Volume sonore maximal)
 * Douceur (Volume sonore minimal)
 * Souplesse (Variation rapide du volume sonore)
 * Agilité (variation rapide de l'intonation, vocalises)
 * Ambitus
 * Souffle (Capacité à faire durer une phrase)
 * Diction
 * Prononciation
 * Equilibre (Savoir garder un timbre naturel)
 * Endurance physique


Toutes ces qualités sont souvent antagonistes :
* Force,Douceur vs Souffle (Chanter fort/doux affecte la consommation d'air et/ou la pression)
* Diction/Ambitus (On sacrifie bien souvent la clarté d'une voyelle pour atteindre un aigu)
* Douceur/Ambitus (Difficile de sortir certain aigus sans un minimum de puissance/à moins de changer de mécanisme)
* Puissance/Endurance

Tout cela fait que la technique vocale est finalement l'apprentissage d'une
économie. Elle a pour objet la gestion des ressources limitées du chanteur afin
d'optimiser l'ensemble de ses qualités vocales.

## L'instrument

L'instrument du chanteur est son propre corps. On peut y distinguer plusieurs
parties qui contribuent à la production du chant.

 - L'appareil respiratoire
 - Les vibrateurs
 - Les résonateurs
 - Les articulateurs

Ces quatres parties doivent être travaillées indépendament d'une part pour
bien reconnaitre leur spécificité; et ensemble d'autre part afin d'exercer
leur coordination.

Pour faire une image, on peut voir chacune de ces parties comme les differents
étages qui constituent l'édifice qu'est le corps du chanteur. Chaque
niveau est porté par celui du dessous, porte les étages du dessus. On cherche
alors à construire l'édifice le plus haut, le plus beau et bien sûr le plus
solide.

Enfin, il y une cinquième partie préalable aux quatres suscitées. Bien qu'elle
n'intervienne pas directement dans la production du son, elle est pourtant
essentielle au developpement du chant :

### La posture

Dire que la posture n'intervient pas directement dans la production du son est
presque un abus de language. Pour filer la métaphore, la posture tiendrait lieu
de fondations. Une fois les fondations terminées, on ne voit toujours rien de
l'édifice, pourtant il est impossible de construire une grande voix sans bonne
posture.

Dit autrement, si l'action des quatres parties (souffle, vibrateurs, ...) est
positif car leur developpement va améliorer la voix du chanteur, celui de la
posture est négatif car, à l'inverse, les défauts de posture vont la brider.


D'abord les pieds: poids également réparti sur l'ensemble du pied

Le bassin doit etre libre pour ne pas entraver la respiration:
  - Déblocage des genoux
  - Corps en Z (reformuler !)
  - Robuste et résistant aux déséquilibres
  - Poids du corps légerement sur l'avant des pieds (méfiance, équilibre dynamique)

Le haut du corps (preciser, buste) doit permettre une respiration profonde et efficace
  - Dos légèrement cambré (méfiance)
  - Epaules en arrière (méfiance)
  - Cage thoracique ouverte (ok), poitrine en avant (méfiance)


La tête et le cou ne doivent pas induire de tensions autour de l'instrument
  - Tète bien alignée à la colonne vertébrale, donc
    - légèrement en avant, menton légèrement abaissé (méfiance)
    - Le regard droit aura tendance à pointer le sol à quelques metres (méfiance)
      - Ne pas hésiter à relever le regard (mais seulement le regard)
  - On doit pouvoir appuyer (pas soi-même!) fort sur le front sans affecter la
	posture (voir Robuste et résistant aux déséquilibres)


### Souffle/Respiration

Base de la respiration:

En règle générale, dans la pratique du chant, l'inspiration doit etre plutôt détendue, alors que l'expiration implique une certaine activité
- Inspiration:
L'entrée de l'air doit coloniser le corps de bas en haut (du périnée jusqu'au
thorax qu'il termine d'ouvrir), dans toute sa largeur (côte flottantes) et dans
toute sa profondeur (nombril-lombaires).\
Ce faisant, les muscles du périnée emmagasinent une tension qui sera
moteur à l'expiration.
- La sortie de l'air (commencer par le périnée) doit se faire en conservant le thorax ouvert. L'air se
vide par la remontée du diaphragme(mais le plus tard), non par l'effondrement du thorax.\
La pression d'expiration dit s'appuyer (repenser) sur le périnée, la rentrée du ventre
est une conséquence de la remontée du diaphragme, non sa cause.

Autres:

- Temporalité de la prise d'air
  - seringue


### Emission

### Résonance (avec Emission?)

### Articulation



